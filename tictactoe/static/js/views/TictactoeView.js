'use strict';
var TictactoeView = Backbone.View.extend({
    squares: ['.zero', '.one', '.two', '.three', '.four', '.five', '.six', '.seven', '.eight'],
    render: function() {
        var boardState = this.model.get('moves');
        if(boardState) {
            for(var i = 0; i < boardState.length; i++) {
                var square = boardState[i];
                //even-indexed moves are X-es, odd-indexed moves are O-es
                if(i % 2 === 0) {
                    this.$el.find(this.squares[square]).addClass('X');
                } else {
                    this.$el.find(this.squares[square]).addClass('O');
                }
            }
        }
    },
    events: {
        'click': 'playMove'
    },

    playMove: function(e) {
        //if there's no id, the game hasn't started
        var id = this.model.get('id');
        //if there's a winner, the game has ended
        var winner = this.model.get('winner');

        //in either case, don't allow a move to be played
        if(id && !winner) {
            var $clickedSquare = $(e.target);
            if($clickedSquare.hasClass('board-square-content')) {
                $clickedSquare = $($clickedSquare.parent());
            }
            var move = $clickedSquare.data('value');
            this.model.addMove(move);
        }
    },
    initialize: function() {
        this.listenTo(this.model, 'change:moves', this.render);
    }
});
