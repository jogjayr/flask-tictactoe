'use strict';

var boardView = new TictactoeView({
    el: '#game-board',
    model: new TictactoeModel()
});

//Singleton view for the app
var appView = new (Backbone.View.extend({
    el: '#game-info',
    model: boardView.model,
    startGame: function() {
        this.model.save();
    },
    events: {
        'click .start_game': 'startGame'
    },
    updatePlayerTurn: function() {
        var moves = this.model.get('moves');
        var whoseTurn = moves.length % 2 === 0 ? 'A' : 'B';
        
        //this could be done with a template
        this.$el.text('Your turn Player ' + whoseTurn);
    },
    updateGameResult: function() {
        var winner = this.model.get('winner');
        if(winner) {
            if(winner !== 'Draw') {
                this.$el.text('Congrats player ' + winner + ' !');
            } else {
                this.$el.text('How boring! You had a draw');
            }
        } 
    },
    initialize: function() {
        this.listenTo(this.model, 'change:moves', this.updatePlayerTurn);
        this.listenTo(this.model, 'change:winner', this.updateGameResult);
    }
}));
