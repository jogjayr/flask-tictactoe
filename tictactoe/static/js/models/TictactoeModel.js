var TictactoeModel = Backbone.Model.extend({
    constructor: function() {
        Backbone.Model.apply(this, arguments);
    },
    moves: [],
    urlRoot: '/tictactoe/game',
    addMove: function(move) {
        //Push a new move into the model
        var moves = this.get('moves');
        if(moves.indexOf(move) === -1) {
            moves.push(move);
            //and save the changes
            this.save({'moves': moves});
            if(moves.length >= 1) {
                this.trigger('change:moves');
            }
        }
    }
});
