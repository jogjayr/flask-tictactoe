# -*- coding: utf-8 -*-
from tictactoe import app
from models import Tictactoe

from flask import jsonify
from flask import make_response
from flask import render_template
from flask import request

@app.route('/')
def game_page():
    return render_template('index.html')

@app.route('/tictactoe/game', methods=['POST'])
def create_game():
    game = Tictactoe()
    return jsonify(game.to_dict())

@app.route('/tictactoe/game/<int:id>', methods=['GET'])
def get_game(id):
    game = Tictactoe.get(id)
    if game:
        return jsonify(game.to_dict())
    else:
        return ('No such game', 404)

@app.route('/tictactoe/game/<int:id>', methods=['PUT'])
def register_move(id):
    game = Tictactoe.get(id)
    if game is None:
        return ('No such game', 404)
    moves = request.json['moves']
    game = game.update_moves(moves)
    return jsonify(game.to_dict())
