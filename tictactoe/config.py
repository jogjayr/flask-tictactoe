# -*- coding: utf-8 -*-

import os

ENV = os.environ.get('TTT_ENV')

if ENV == 'test':
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
else:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///data.db'
