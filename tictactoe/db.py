# -*- coding: utf-8 -*-

from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy()

# create the database
def bootstrap(app):
    with app.app_context():
        db.create_all()