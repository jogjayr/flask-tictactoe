# -*- coding: utf-8 -*-
from db import db
from sqlalchemy_utils import ScalarListType

from errors import MoveValidationError

# If a player's move history contains any one of these,
# that player has won
winning_combinations = [(0, 1, 2), (3, 4, 5), (6, 7, 8),
                        (0, 4, 8), (2, 4, 6),
                        (0, 3, 6), (1, 4, 7), (2, 5, 8)]

class Tictactoe(db.Model):
    '''
    This is the game board

    0|1|2
    -----
    3|4|5
    -----
    6|7|8
    
    '''
    __tablename__ = 'tictactoe'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    moves = db.Column(ScalarListType(int), default=[])
    winner = db.Column(db.String)


    def __init__(self):
        self.moves = None
        self.winner = None
        db.session.add(self)
        db.session.commit()

    def to_dict(self):
        ret = {}
        for column in self.__table__.columns:
            ret[column.name] = getattr(self, column.name)
        return ret

    def get_winner(self):
        '''
        Returns winner of tictactoe game after looking at
        moves record
        :return 'A' if winner is player A, 'B' if winner is player B
                'Draw' if game is over and no winner, None otherwise
        '''
        # it's assumed that X plays first, so all even indexed
        # moves are X-es and odds are O-es
        move_list = self.moves
        x_es, o_es = [], []
        for i in xrange(len(move_list)):   
            if i % 2 == 0:
                x_es.append(move_list[i])
            else:
                o_es.append(move_list[i])

        if self._contains_winning_move(x_es):
            return 'A'
        elif self._contains_winning_move(o_es):
            return 'B'
        elif len(move_list) == 9:
            return 'Draw'
        else:
            return None

    def _contains_winning_move(self, move_list):
        '''
        :param move_list: List of squares played
        :return True if move_list contains a winning move,
                False otherwise
        '''
        if len(move_list) < 3:
            return False

        move_list = sorted(move_list)

        for combination in winning_combinations:
            contains_combination = reduce(lambda result, square: result and (square in move_list),
                                          combination, True)
            if contains_combination:
                return True
        return False

    @classmethod
    def get(cls, id):
        return cls.query.get(id)

    def update_moves(self, updated_moves):
        '''
        Add a move to the move list and store in db
        :param move: An integer between 0 and 8
                     that hasn't been played before
        :return The winner of the game in this state
        '''
        move = updated_moves[-1]
        if move > 8 or move < 0:
            raise MoveValidationError('Move can have values 0-8 inclusive')

        if move in self.moves:
            raise MoveValidationError('Move already played')
        else:
            self.moves = updated_moves

        self.winner = self.get_winner()
        try:
            db.session.add(self)
            db.session.commit()
        except Exception as e:
            print e
        return self
