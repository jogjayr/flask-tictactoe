# -*- coding: utf-8 -*-

from flask import Flask
from flask import jsonify
from config import SQLALCHEMY_DATABASE_URI
from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException
from db import db
from db import bootstrap
__all__ = ['make_json_app']


#http://flask.pocoo.org/snippets/83/
def make_json_app(import_name, **kwargs):
    """
    Creates a JSON-oriented Flask app.

    All error responses that you don't specifically
    manage yourself will have application/json content
    type, and will contain JSON like this (just an example):

    { "message": "405: Method Not Allowed" }
    """
    def make_json_error(ex):
        response = jsonify(message=str(ex))
        response.status_code = (ex.code
                                if isinstance(ex, HTTPException)
                                else 500)
        return response

    app = Flask(import_name, **kwargs)

    for code in default_exceptions.iterkeys():
        app.error_handler_spec[None][code] = make_json_error

    app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
    db.init_app(app)
    from models import Tictactoe
    bootstrap(app)
    return app

app = make_json_app(__name__)

