# -*- coding: utf-8 -*-
from tictactoe import app
from tictactoe.db import bootstrap
from tictactoe.models import Tictactoe
from nose.tools import eq_

def setUp():
    with app.app_context():
        bootstrap(app)

def test_winner():
    with app.app_context():
        ttt = Tictactoe()
        ttt.update_moves([0, 3, 1, 5, 2])
        eq_(ttt.get_winner(), 'A')
        ttt.update_moves([0, 1, 3, 2, 4, 5, 6])
        eq_(ttt.get_winner(), 'A')
        ttt.update_moves([0, 2, 3, 5, 4, 7, 8])
        eq_(ttt.get_winner(), 'A')

