## Dependencies

    python2.7
    virtualenv
    sqlite3
    bower

## To run

    ./run #this will install Python and bower libs

## To play

Go to http://127.0.0.1:5000/

## Python tests

    ./test

